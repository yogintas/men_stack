var users = require('../../app/controllers/users.server.controller'),
	passport = require('passport');

module.exports = function(app) {
	app.route('/users').post(users.create).get(users.list);

	app.route('/users/:userId').get(users.read).put(users.update).delete(users.delete);

	app.param('userId', users.userByID);

	app.route('/register')
		.get(users.renderRegister)
		.post(users.register);

	app.route('/login')		
		.get(users.renderLogin)
		.post(passport.authenticate('local', {
			successRedirect: '/get',
			failureRedirect: '/login',
			failureFlash: true
		}));

	app.get('/logout', users.logout);
	
	app.route('/get')
		.get(users.getDevices);

	app.route('/update')
		.post(users.update)

	app.get('/oauth/facebook', passport.authenticate('facebook', {
		failureRedirect: '/login',
		scope:['email']
	}));

	app.get('/oauth/facebook/callback', passport.authenticate('facebook', {
		failureRedirect: '/login',
		successRedirect: '/get',
		scope:['email']
	}));
}