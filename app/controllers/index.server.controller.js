var users = require('../../app/controllers/users.server.controller')

exports.render = function(req, res, next) {	
    res.render('index', {
    	title: 'Pradinis',
    	user: req.user ? req.user.name: '',
    	device: users.devices
    })   
};